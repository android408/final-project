package com.panuwat.acebook

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.panuwat.acebook.databinding.FragmentPage1Binding
import com.panuwat.acebook.databinding.FragmentPage4Binding
import com.panuwat.acebook.databinding.FragmentStartBinding

class Page4Fragment : Fragment() {
    private var binding: FragmentPage4Binding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentPage4Binding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            page4Fragment = this@Page4Fragment
        }
    }
}