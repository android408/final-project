package com.panuwat.acebook

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.panuwat.acebook.databinding.FragmentMainBinding

class MainFragment : Fragment() {
    private var binding: FragmentMainBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentMainBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            mainFragment = this@MainFragment
            lifecycleOwner = viewLifecycleOwner
        }
    }

    fun goToNextScreen(){
        findNavController().navigate(R.id.action_mainFragment_to_page1Fragment)
    }
    fun goToNextScreen1(){
        findNavController().navigate(R.id.action_mainFragment_to_page2Fragment22)
    }
    fun goToNextScreen2(){
        findNavController().navigate(R.id.action_mainFragment_to_page3Fragment2)
    }
    fun goToNextScreen3(){
        findNavController().navigate(R.id.action_mainFragment_to_page4Fragment)
    }
    fun goToNextScreen4(){
        findNavController().navigate(R.id.action_mainFragment_to_page5Fragment)
    }
    fun goToNextScreen5(){
        findNavController().navigate(R.id.action_mainFragment_to_page6Fragment)
    }

}